package com.maurevair.booking;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@SpringBootTest
class MaurevAirApplicationTests {

  @Test
  void test() {
    String dateTimeString = "2022-05-01T12:08:00.000+04:00";
    LocalDateTime time = LocalDateTime.of(2022, 5, 1, 12, 8, 0, 0);
    OffsetDateTime offsetDateTime = OffsetDateTime.parse(dateTimeString, DateTimeFormatter.ISO_OFFSET_DATE_TIME);

    System.out.println("OffsetDateTime: " + offsetDateTime);
    System.out.println(time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)));
  }

}
