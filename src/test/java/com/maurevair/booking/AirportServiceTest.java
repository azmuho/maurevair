package com.maurevair.booking;

import com.maurevair.booking.domain.AirportEntity;
import com.maurevair.booking.domain.FlightEntity;
import com.maurevair.booking.dto.AirportDetailsDto;
import com.maurevair.booking.dto.FlightDto;
import com.maurevair.booking.repository.AirportRepository;
import com.maurevair.booking.repository.FlightRepository;
import com.maurevair.booking.service.AirportService;
import com.maurevair.booking.type.AirportCode;
import com.maurevair.booking.type.Carrier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AirportServiceTest {

    @Mock
    private AirportRepository airportRepository;

    @Mock
    private FlightRepository flightRepository;

    @InjectMocks
    private AirportService airportService;

    private AirportEntity airportEntity;
    private FlightEntity flightEntity;

    @BeforeEach
    void setUp() {
        airportEntity = AirportEntity.builder()
                .id(1L)
                .airportCode(AirportCode.MRU)
                .city("Plaisance")
                .region("Grand Port")
                .country("Mauritius")
                .build();

        flightEntity = FlightEntity.builder()
                .id("ID0001")
                .carrier(Carrier.MRV)
                .flightNumber("F123")
                .origin(AirportCode.MRU)
                .destination(AirportCode.RUN)
                .arrivalTime(OffsetDateTime.now().plusHours(1))
                .departureTime(OffsetDateTime.now())
                .build();
    }

    @Test
    void testGetAirportDetails() {
        when(airportRepository.findAll()).thenReturn(Collections.singletonList(airportEntity));
        when(flightRepository.findByOrigin(any())).thenReturn(Collections.singletonList(flightEntity));

        List<AirportDetailsDto> airportDetails = airportService.getAirportDetails();

        assertNotNull(airportDetails);
        assertEquals(1, airportDetails.size());

        AirportDetailsDto airportDetailsDto = airportDetails.get(0);
        assertEquals(1L, airportDetailsDto.getId());
        assertEquals(AirportCode.MRU, airportDetailsDto.getAirportCode());
        assertEquals("Plaisance, Grand Port, Mauritius", airportDetailsDto.getPlace());
        assertEquals(1, airportDetailsDto.getFlights().size());

        FlightDto flightDto = airportDetailsDto.getFlights().get(0);
        assertEquals("ID0001", flightDto.getId());
        assertEquals(Carrier.MRV, flightDto.getCarrier());
        assertEquals("F123", flightDto.getFlightNumber());
        assertEquals(AirportCode.MRU, flightDto.getOrigin());
        assertEquals(AirportCode.RUN, flightDto.getDestination());
    }
}
