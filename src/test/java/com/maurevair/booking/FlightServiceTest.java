package com.maurevair.booking;

import com.maurevair.booking.domain.BookingInfoEntity;
import com.maurevair.booking.domain.FlightEntity;
import com.maurevair.booking.dto.BookingDto;
import com.maurevair.booking.dto.FlightDto;
import com.maurevair.booking.helper.BookingInfo;
import com.maurevair.booking.helper.Flight;
import com.maurevair.booking.helper.FlightsData;
import com.maurevair.booking.repository.BookingInfoRepository;
import com.maurevair.booking.repository.FlightRepository;
import com.maurevair.booking.service.FlightService;
import com.maurevair.booking.type.AirportCode;
import com.maurevair.booking.type.CabinClass;
import com.maurevair.booking.type.Carrier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FlightServiceTest {

    @Mock
    private JAXBContext jaxbContext;

    @Mock
    private Unmarshaller unmarshaller;

    @Mock
    private FlightRepository flightRepository;

    @Mock
    private BookingInfoRepository bookingInfoRepository;

    @InjectMocks
    private FlightService flightService;

    private final AirportCode origin = AirportCode.MRU;
    private final AirportCode destination = AirportCode.RUN;

    private FlightEntity flightEntity;
    private BookingInfoEntity bookingInfoEntity;
    private BookingDto bookingDto;

    @BeforeEach
    void setUp() throws JAXBException {
        buildHelperObjects();
        buildEntities();

//        when(jaxbContext.createUnmarshaller()).thenReturn(unmarshaller);
//        when(unmarshaller.unmarshal(any(File.class))).thenReturn(flightsData);
    }

    private void buildHelperObjects() {
        Flight flight = new Flight();
        flight.setId("ID0002");
        flight.setCarrier("MRV");
        flight.setFlightNumber("F123");
        flight.setOrigin("MRU");
        flight.setDestination("CDG");
        flight.setArrivalTime("2024-06-01T10:05:00.000+02:00");
        flight.setDepartureTime("2024-06-02T10:05:00.000+02:00");

        BookingInfo bookingInfo = new BookingInfo();
        bookingInfo.setCabinClass("Economy");
        bookingInfo.setSeatsAvailable(100);

        FlightsData flightsData = new FlightsData();
        flightsData.setFlights(Collections.singletonList(flight));
    }

    private void buildEntities() {
        bookingInfoEntity = BookingInfoEntity.builder()
                .flight(flightEntity)
                .seatsAvailable(10)
                .cabinClass(CabinClass.ECONOMY)
                .build();

        flightEntity = FlightEntity.builder()
                .id("ID0001")
                .carrier(Carrier.MRV)
                .flightNumber("F123")
                .origin(origin)
                .destination(destination)
                .arrivalTime(OffsetDateTime.now().plusHours(1))
                .departureTime(OffsetDateTime.now())
                .bookingInfo(Collections.singleton(bookingInfoEntity))
                .build();

        bookingDto = BookingDto.builder()
                .flightId("F123")
                .cabinClass(CabinClass.ECONOMY)
                .seats(2)
                .build();
    }

    @Test
    void testImportFlights() throws JAXBException {
        when(flightRepository.save(any(FlightEntity.class))).thenReturn(flightEntity);
        when(bookingInfoRepository.saveAll(anyList())).thenReturn(Collections.singletonList(bookingInfoEntity));

        flightService.importFlights();

        verify(flightRepository, times(10)).save(any(FlightEntity.class));
        verify(bookingInfoRepository, times(10)).saveAll(anyList());
    }

    @Test
    void testGetFlightsByRoute() {
        when(flightRepository.findByOriginAndDestination(origin, destination))
                .thenReturn(Collections.singletonList(flightEntity));

        List<FlightDto> flightDtoList = flightService.getFlightsByRoute(origin, destination);

        assertEquals(1, flightDtoList.size());
        FlightDto dto = flightDtoList.get(0);
        assertEquals(flightEntity.getId(), dto.getId());
        assertEquals(flightEntity.getCarrier(), dto.getCarrier());
        assertEquals(flightEntity.getFlightNumber(), dto.getFlightNumber());
        assertEquals(flightEntity.getOrigin(), dto.getOrigin());
        assertEquals(flightEntity.getDestination(), dto.getDestination());
        assertEquals(flightEntity.getArrivalTime(), dto.getArrivalTime());
        assertEquals(flightEntity.getDepartureTime(), dto.getDepartureTime());
        assertEquals(flightEntity.getBookingInfo().size(), dto.getBookingInfos().size());
    }

    @Test
    void testBookFlight() {
        when(flightRepository.findById("F123")).thenReturn(Optional.of(flightEntity));
        when(bookingInfoRepository.findByFlightAndCabinClass(flightEntity, CabinClass.ECONOMY)).thenReturn(Optional.of(bookingInfoEntity));

        flightService.bookFlight(bookingDto);

        verify(bookingInfoRepository, times(1)).save(bookingInfoEntity);
        assertEquals(8, bookingInfoEntity.getSeatsAvailable());
    }

    @Test
    void testBookFlightFlightNotFound() {
        when(flightRepository.findById("F123")).thenReturn(Optional.empty());

        EntityNotFoundException exception = assertThrows(EntityNotFoundException.class, () -> {
            flightService.bookFlight(bookingDto);
        });

        assertEquals("No flight found with id: F123", exception.getMessage());
    }

    @Test
    void testBookFlightBookingInfoNotFound() {
        when(flightRepository.findById("F123")).thenReturn(Optional.of(flightEntity));
        when(bookingInfoRepository.findByFlightAndCabinClass(flightEntity, CabinClass.ECONOMY)).thenReturn(Optional.empty());

        EntityNotFoundException exception = assertThrows(EntityNotFoundException.class, () -> {
            flightService.bookFlight(bookingDto);
        });

        assertEquals("The cabin class Economy does not exist for flight F123!", exception.getMessage());
    }

    @Test
    void testBookFlightNoSeatsAvailable() {
        bookingDto.setSeats(11);
        when(flightRepository.findById("F123")).thenReturn(Optional.of(flightEntity));
        when(bookingInfoRepository.findByFlightAndCabinClass(flightEntity, CabinClass.ECONOMY)).thenReturn(Optional.of(bookingInfoEntity));

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            flightService.bookFlight(bookingDto);
        });

        assertEquals("Only 10 seats available for flight F123 Economy class!", exception.getMessage());
    }
}
