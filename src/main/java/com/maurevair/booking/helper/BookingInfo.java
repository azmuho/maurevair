package com.maurevair.booking.helper;

import lombok.Setter;

import javax.xml.bind.annotation.XmlAttribute;

@Setter
public class BookingInfo {
    private String cabinClass;
    private int seatsAvailable;

    @XmlAttribute(name = "CabinClass")
    public String getCabinClass() {
        return cabinClass;
    }

    @XmlAttribute(name = "SeatsAvailable")
    public int getSeatsAvailable() {
        return seatsAvailable;
    }

}