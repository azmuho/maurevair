package com.maurevair.booking.helper;

import lombok.Setter;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "flight")
@Setter
public class Flight {
    private String id;
    private String carrier;
    private String flightNumber;
    private String origin;
    private String destination;
    private String departureTime;
    private String arrivalTime;
    private List<BookingInfo> bookingInfo;

    @XmlAttribute
    public String getId() {
        return id;
    }

    @XmlAttribute(name = "Carrier")
    public String getCarrier() {
        return carrier;
    }

    @XmlAttribute(name = "FlightNumber")
    public String getFlightNumber() {
        return flightNumber;
    }

    @XmlAttribute(name = "Origin")
    public String getOrigin() {
        return origin;
    }

    @XmlAttribute(name = "Destination")
    public String getDestination() {
        return destination;
    }

    @XmlAttribute(name = "DepartureTime")
    public String getDepartureTime() {
        return departureTime;
    }

    @XmlAttribute(name = "ArrivalTime")
    public String getArrivalTime() {
        return arrivalTime;
    }

    @XmlElement(name = "BookingInfo")
    public List<BookingInfo> getBookingInfo() {
        return bookingInfo;
    }

}
