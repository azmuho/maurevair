package com.maurevair.booking.helper;

import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "flights")
@Setter
public class FlightsData {
    private List<Flight> flights;

    @XmlElement(name = "flight")
    public List<Flight> getFlights() {
        return flights;
    }
}
