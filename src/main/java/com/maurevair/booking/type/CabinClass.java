package com.maurevair.booking.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CabinClass {
    FIRST("First"),
    PREMIUM_ECONOMY("PremiumEconomy"),
    ECONOMY("Economy");

    private final String name;

    public static CabinClass getValueFromString(String name) {
        for (CabinClass cabinClass : CabinClass.values()) {
            if (cabinClass.name.equalsIgnoreCase(name)) {
                return cabinClass;
            }
        }
        throw new IllegalArgumentException("Invalid cabin class: " + name);
    }
}
