package com.maurevair.booking.type;

public enum AirportCode {
    MRU,
    CDG,
    ORY,
    RUN,
    ZSE,
    TNR,
    LHR,
    SEZ,
    RRG,
    SIN

}
