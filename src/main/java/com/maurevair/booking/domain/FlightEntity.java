package com.maurevair.booking.domain;

import com.maurevair.booking.type.AirportCode;
import com.maurevair.booking.type.Carrier;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.OffsetDateTime;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "flight")
public class FlightEntity {

    @Id
    private String id;

    @NonNull
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 3)
    private Carrier carrier;

    @NonNull
    @Column(nullable = false)
    private String flightNumber;

    @NonNull
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 3)
    private AirportCode origin;

    @NonNull
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 3)
    private AirportCode destination;

    @NonNull
    @Column(nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime departureTime;

    @NonNull
    @Column(nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime arrivalTime;

    @Transient
    private Set<BookingInfoEntity> bookingInfo;
}

