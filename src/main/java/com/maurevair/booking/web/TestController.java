package com.maurevair.booking.web;

import com.maurevair.booking.domain.WelcomeMessage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/v2")
public class TestController {

    @GetMapping(value = "/welcome")
    public WelcomeMessage getGreeting() {
        WelcomeMessage welcomeMessage = new WelcomeMessage("Welcome to MaurevAir test");
        return welcomeMessage;
    }
}
