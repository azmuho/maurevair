package com.maurevair.booking.web;

import com.maurevair.booking.domain.WelcomeMessage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;

import static com.maurevair.booking.config.UserRoles.Fields.ADMIN;
import static com.maurevair.booking.config.UserRoles.Fields.USER;

@RestController
@RequestMapping(path = "/api/")
public class InfoController {

    @GetMapping(value = "admin/welcome")
    @RolesAllowed(ADMIN)
    public WelcomeMessage getGreeting() {
        WelcomeMessage welcomeMessage = new WelcomeMessage("Welcome to MaurevAir Admin");
        return welcomeMessage;
    }

    @GetMapping(value = "user/welcome")
    @RolesAllowed(USER)
    public WelcomeMessage getGreetingUser() {
        WelcomeMessage welcomeMessage = new WelcomeMessage("Welcome to MaurevAir User");
        return welcomeMessage;
    }
}
