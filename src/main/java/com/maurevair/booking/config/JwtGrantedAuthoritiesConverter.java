package com.maurevair.booking.config;

import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class JwtGrantedAuthoritiesConverter implements Converter<Jwt, Collection<GrantedAuthority>> {

    private final String clientId;

    public JwtGrantedAuthoritiesConverter(String clientId) {
        this.clientId = clientId;
    }

    public Collection<GrantedAuthority> convert(Jwt source) {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        Optional<Map<String, Object>> resourceAccessOpt = Optional.ofNullable(source)
                .map(jwt -> jwt.getClaim("resource_access"));

        if (resourceAccessOpt.isPresent()) {
            Map<String, Object> resourceAccess = resourceAccessOpt.get();
            Map<String, Object> clientAccess = (Map<String, Object>) resourceAccess.get(clientId);

            if (clientAccess != null) {
                List<String> roles = (List<String>) clientAccess.get("roles");

                if (roles != null) {
                    for (String role : roles) {
                        String formattedRole = String.format("ROLE_%s", role).toUpperCase();
                        authorities.add(new SimpleGrantedAuthority(formattedRole));
                    }
                }
            }
        }
        return authorities;
    }
}
