package com.maurevair.booking.config;

import lombok.experimental.FieldNameConstants;
import lombok.experimental.FieldNameConstants.Include;

@FieldNameConstants(onlyExplicitlyIncluded = true)
public enum UserRoles {

    @Include
    ADMIN,

    @Include
    USER;
}
