package com.maurevair.booking.repository;

import com.maurevair.booking.domain.FlightEntity;
import com.maurevair.booking.type.AirportCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlightRepository extends JpaRepository<FlightEntity, String> {

    List<FlightEntity> findByOrigin(AirportCode origin);

    List<FlightEntity> findByOriginAndDestination(AirportCode origin, AirportCode destination);
}
