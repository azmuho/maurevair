package com.maurevair.booking.repository;

import com.maurevair.booking.domain.BookingInfoEntity;
import com.maurevair.booking.domain.FlightEntity;
import com.maurevair.booking.type.CabinClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface BookingInfoRepository extends JpaRepository<BookingInfoEntity, String> {

    Set<BookingInfoEntity> findByFlight(FlightEntity flightEntity);

    Optional<BookingInfoEntity> findByFlightAndCabinClass(FlightEntity flightEntity, CabinClass cabinClass);
}
