package com.maurevair.booking.repository;

import com.maurevair.booking.domain.AirportEntity;
import com.maurevair.booking.type.AirportCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AirportRepository extends JpaRepository<AirportEntity, Long> {

    Optional<AirportEntity> findByAirportCode(AirportCode airportCode);
}
