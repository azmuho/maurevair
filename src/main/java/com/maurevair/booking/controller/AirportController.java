package com.maurevair.booking.controller;

import com.maurevair.booking.dto.AirportDetailsDto;
import com.maurevair.booking.service.AirportService;
import com.maurevair.booking.type.AirportCode;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.util.List;

import static com.maurevair.booking.config.UserRoles.Fields.ADMIN;
import static com.maurevair.booking.config.UserRoles.Fields.USER;

@RestController
@RequestMapping(path = "/api/v1/airport/")
@RequiredArgsConstructor
public class AirportController {

    private final AirportService airportService;

    @Operation(description = "Import airport data from csv file")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Airports imported successfully"),
            @ApiResponse(responseCode = "403", description = "Forbidden - User does not have the necessary permissions"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    @PostMapping("import")
    @RolesAllowed(ADMIN)
    public void importAirportInfo() {
        airportService.importAirportInfo();
    }

    @Operation(description = "Get airport details")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation",
                    content = @Content(schema = @Schema(implementation = AirportDetailsDto.class))),
            @ApiResponse(responseCode = "403", description = "Forbidden - User does not have the necessary permissions"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    @GetMapping("all")
    @RolesAllowed(USER)
    public List<AirportDetailsDto> getAirportDetails() {
        return airportService.getAirportDetails();
    }

    @Operation(description = "Get details of an airport by its code")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation",
                    content = @Content(schema = @Schema(implementation = AirportDetailsDto.class))),
            @ApiResponse(responseCode = "403", description = "Forbidden - User does not have the necessary permissions"),
            @ApiResponse(responseCode = "404", description = "Not found - Airport not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    @GetMapping("{airportCode}")
    @RolesAllowed(USER)
    public AirportDetailsDto getAirportDetailsByCode(@PathVariable AirportCode airportCode) {
        return airportService.getAirportDetailsByCode(airportCode);
    }
}
