package com.maurevair.booking.controller;

import com.maurevair.booking.dto.BookingDto;
import com.maurevair.booking.dto.FlightDto;
import com.maurevair.booking.service.FlightService;
import com.maurevair.booking.type.AirportCode;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import javax.xml.bind.JAXBException;
import java.util.List;

import static com.maurevair.booking.config.UserRoles.Fields.ADMIN;
import static com.maurevair.booking.config.UserRoles.Fields.USER;

@RestController
@RequestMapping(path = "/api/v1/flight/")
@RequiredArgsConstructor
@Slf4j
public class FlightController {

    private final FlightService flightService;

    @Operation(description = "Import flight data")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Flights imported successfully"),
            @ApiResponse(responseCode = "403", description = "Forbidden - User does not have the necessary permissions"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    @PostMapping("import")
    @RolesAllowed(ADMIN)
    public void importFlights() {
        try {
            flightService.importFlights();
        } catch (JAXBException e) {
            log.error("An error occurred while getting flights data", e);
        }
    }

    @Operation(description = "Get flights by route")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation",
                    content = @Content(schema = @Schema(implementation = FlightDto.class))),
            @ApiResponse(responseCode = "400", description = "Bad request - Invalid airport codes"),
            @ApiResponse(responseCode = "404", description = "Not found - Airport not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    @GetMapping("by-route")
    @RolesAllowed(USER)
    public List<FlightDto> getFlightsByRoute(@RequestParam AirportCode origin, @RequestParam AirportCode destination) {
        return flightService.getFlightsByRoute(origin, destination);
    }

    @Operation(description = "Book a flight")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Flight booked successfully"),
            @ApiResponse(responseCode = "400", description = "Bad request - Invalid booking data"),
            @ApiResponse(responseCode = "403", description = "Forbidden - User does not have the necessary permissions"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    @PutMapping("book-flight")
    @RolesAllowed(USER)
    public void bookFlight(@RequestBody BookingDto bookingDto) {
        flightService.bookFlight(bookingDto);
    }
}
