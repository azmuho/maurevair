package com.maurevair.booking.service;

import com.maurevair.booking.domain.BookingInfoEntity;
import com.maurevair.booking.domain.FlightEntity;
import com.maurevair.booking.dto.BookingDto;
import com.maurevair.booking.dto.BookingInfoDto;
import com.maurevair.booking.dto.FlightDto;
import com.maurevair.booking.helper.BookingInfo;
import com.maurevair.booking.helper.Flight;
import com.maurevair.booking.helper.FlightsData;
import com.maurevair.booking.repository.BookingInfoRepository;
import com.maurevair.booking.repository.FlightRepository;
import com.maurevair.booking.type.AirportCode;
import com.maurevair.booking.type.CabinClass;
import com.maurevair.booking.type.Carrier;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FlightService {

    private final FlightRepository flightRepository;
    private final BookingInfoRepository bookingInfoRepository;

    public void importFlights() throws JAXBException {

        FlightsData flightsData = unmarshallFlights();

        if (!Objects.isNull(flightsData)) {
            flightsData.getFlights().forEach(flight -> {
                FlightEntity flightEntity = flightRepository.save(buildFlightEntity(flight));
                bookingInfoRepository.saveAll(buildBookingInfoEntityList(flight.getBookingInfo(), flightEntity));

            });
        }
    }

    private FlightsData unmarshallFlights() throws JAXBException {
        File xml = new File("etc/flights.xml");

        JAXBContext jaxbContext = JAXBContext.newInstance(FlightsData.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return (FlightsData) unmarshaller.unmarshal(xml);
    }

    private FlightEntity buildFlightEntity(Flight flight) {
        return FlightEntity.builder()
                .id(flight.getId())
                .carrier(Carrier.valueOf(flight.getCarrier()))
                .flightNumber(flight.getFlightNumber())
                .origin(AirportCode.valueOf(flight.getOrigin()))
                .destination(AirportCode.valueOf(flight.getDestination()))
                .arrivalTime(convertStringToOffsetDateTime(flight.getArrivalTime()))
                .departureTime(convertStringToOffsetDateTime(flight.getDepartureTime()))
                .build();
    }

    private List<BookingInfoEntity> buildBookingInfoEntityList(List<BookingInfo> bookingInfoList, FlightEntity flightEntity) {
        return bookingInfoList.stream()
                .map(bookingInfo -> BookingInfoEntity.builder()
                        .cabinClass(CabinClass.getValueFromString(bookingInfo.getCabinClass()))
                        .seatsAvailable(bookingInfo.getSeatsAvailable())
                        .flight(flightEntity)
                        .build())
                .collect(Collectors.toList());
    }

    private OffsetDateTime convertStringToOffsetDateTime(String date) {
        return OffsetDateTime.parse(date, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public List<FlightDto> getFlightsByRoute(AirportCode origin, AirportCode destination) {
        List<FlightEntity> flightEntityList = flightRepository.findByOriginAndDestination(origin, destination);

        flightEntityList.forEach(flightEntity -> {
            Set<BookingInfoEntity> bookingInfoEntitySet = bookingInfoRepository.findByFlight(flightEntity);
            flightEntity.setBookingInfo(bookingInfoEntitySet);
        });
        return flightEntityList.stream()
                .map(this::buildFlightDto)
                .collect(Collectors.toList());
    }

    private FlightDto buildFlightDto(FlightEntity flightEntity) {
        return FlightDto.builder()
                .id(flightEntity.getId())
                .carrier(flightEntity.getCarrier())
                .flightNumber(flightEntity.getFlightNumber())
                .origin(flightEntity.getOrigin())
                .destination(flightEntity.getDestination())
                .arrivalTime(flightEntity.getArrivalTime())
                .departureTime(flightEntity.getDepartureTime())
                .bookingInfos(mapToBookingInfoDto(flightEntity.getBookingInfo()))
                .build();
    }

    private Set<BookingInfoDto> mapToBookingInfoDto(Set<BookingInfoEntity> bookingInfoEntitySet) {

        return bookingInfoEntitySet.stream().map(
                bookingInfoEntity -> BookingInfoDto.builder()
                        .id(bookingInfoEntity.getId())
                        .cabinClass(bookingInfoEntity.getCabinClass())
                        .seatsAvailable(bookingInfoEntity.getSeatsAvailable())
                        .build()
        ).collect(Collectors.toSet());
    }

    public void bookFlight(BookingDto bookingDto) {
        FlightEntity flightEntity = flightRepository.findById(bookingDto.getFlightId())
                .orElseThrow(() -> new EntityNotFoundException("No flight found with id: " + bookingDto.getFlightId()));

        BookingInfoEntity bookingInfo = bookingInfoRepository.findByFlightAndCabinClass(flightEntity, bookingDto.getCabinClass())
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("The cabin class %s does not exist for flight %s!", bookingDto.getCabinClass().getName(), bookingDto.getFlightId())));

        if (bookingDto.getSeats() > bookingInfo.getSeatsAvailable()) {
            throw new IllegalArgumentException(
                    String.format("Only %d seats available for flight %s %s class!", bookingInfo.getSeatsAvailable(), bookingDto.getFlightId(), bookingDto.getCabinClass().getName()));
        }

        int seatsRemaining = bookingInfo.getSeatsAvailable() - bookingDto.getSeats();
        bookingInfo.setSeatsAvailable(seatsRemaining);
        bookingInfoRepository.save(bookingInfo);
    }
}
