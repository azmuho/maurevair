package com.maurevair.booking.service;

import com.maurevair.booking.domain.AirportEntity;
import com.maurevair.booking.domain.FlightEntity;
import com.maurevair.booking.dto.AirportDetailsDto;
import com.maurevair.booking.dto.AirportDto;
import com.maurevair.booking.dto.FlightDto;
import com.maurevair.booking.repository.AirportRepository;
import com.maurevair.booking.repository.FlightRepository;
import com.maurevair.booking.type.AirportCode;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class AirportService {

    private final AirportRepository airportRepository;
    private final FlightRepository flightRepository;

    public void importAirportInfo() {
        String fileName = "etc/airport_info.csv";

        try (FileReader reader = new FileReader(fileName)) {
            CsvToBean<AirportDto> csvToBean = new CsvToBeanBuilder<AirportDto>(reader)
                    .withType(AirportDto.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .withFieldAsNull(CSVReaderNullFieldIndicator.BOTH)
                    .build();

            List<AirportDto> airports = csvToBean.parse();

            airportRepository.saveAll(buildAirportEntities(airports));

        } catch (IOException e) {
            log.error("An error occurred while reading airport data", e);
        }
    }

    private List<AirportEntity> buildAirportEntities(List<AirportDto> airportDtoList) {
        return airportDtoList.stream()
                .map(airportDto -> AirportEntity.builder()
                        .country(airportDto.getCountry())
                        .region(airportDto.getRegion())
                        .city(airportDto.getCity())
                        .airportCode(airportDto.getAirportCode())
                        .build())
                .collect(Collectors.toList());
    }

    public List<AirportDetailsDto> getAirportDetails() {
        List<AirportEntity> airportEntityList = airportRepository.findAll();

        return airportEntityList.stream()
                .map(this::buildAirportDetails)
                .collect(Collectors.toList());

    }

    private AirportDetailsDto buildAirportDetails(AirportEntity airportEntity) {
        List<FlightDto> flights = getFlightsByOrigin(airportEntity.getAirportCode());
        return mapToAirportDetailsDto(airportEntity, flights);
    }

    private List<FlightDto> getFlightsByOrigin(AirportCode origin) {
        List<FlightEntity> flightEntityList = flightRepository.findByOrigin(origin);

        return flightEntityList.stream()
                .map(this::mapToFlightDto)
                .collect(Collectors.toList());
    }

    private FlightDto mapToFlightDto(FlightEntity flightEntity) {
        return FlightDto.builder()
                .id(flightEntity.getId())
                .carrier(flightEntity.getCarrier())
                .flightNumber(flightEntity.getFlightNumber())
                .origin(flightEntity.getOrigin())
                .destination(flightEntity.getDestination())
                .departureTime(flightEntity.getDepartureTime())
                .arrivalTime(flightEntity.getArrivalTime())
                .build();
    }

    private AirportDetailsDto mapToAirportDetailsDto(AirportEntity airportEntity, List<FlightDto> flights) {
        return AirportDetailsDto.builder()
                .id(airportEntity.getId())
                .airportCode(airportEntity.getAirportCode())
                .place(airportEntity.getCity() + ", " + airportEntity.getRegion() + ", " + airportEntity.getCountry())
                .flights(flights)
                .build();
    }

    public AirportDetailsDto getAirportDetailsByCode(AirportCode airportCode) {
        AirportEntity airportEntity = airportRepository.findByAirportCode(airportCode)
                .orElseThrow(() -> new EntityNotFoundException("No airport found with code: " + airportCode));
        List<FlightDto> flights = getFlightsByOrigin(airportEntity.getAirportCode());
        return mapToAirportDetailsDto(airportEntity, flights);
    }
}
