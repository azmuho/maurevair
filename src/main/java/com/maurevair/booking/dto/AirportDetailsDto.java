package com.maurevair.booking.dto;

import com.maurevair.booking.type.AirportCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AirportDetailsDto {

    private Long id;
    private AirportCode airportCode;
    private String place;
    private List<FlightDto> flights;
}
