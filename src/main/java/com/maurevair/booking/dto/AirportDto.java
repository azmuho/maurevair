package com.maurevair.booking.dto;

import com.maurevair.booking.type.AirportCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AirportDto {

    private Long id;

    @NonNull
    private String country;

    private String region;

    @NonNull
    private String city;

    @NonNull
    private AirportCode airportCode;
}
