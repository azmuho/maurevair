package com.maurevair.booking.dto;

import com.maurevair.booking.type.AirportCode;
import com.maurevair.booking.type.Carrier;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlightDto {

    private String id;

    @NonNull
    private Carrier carrier;

    @NonNull
    private String flightNumber;

    @NonNull
    private AirportCode origin;

    @NonNull
    private AirportCode destination;

    @NonNull
    private OffsetDateTime departureTime;

    @NonNull
    private OffsetDateTime arrivalTime;

    private Set<BookingInfoDto> bookingInfos;
}
