package com.maurevair.booking.dto;

import com.maurevair.booking.type.CabinClass;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookingInfoDto {

    private Long id;

    @NonNull
    private CabinClass cabinClass;

    private int seatsAvailable;

    private FlightDto flight;
}
